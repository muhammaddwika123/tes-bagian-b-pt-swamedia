<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>
    table {
        margin: auto;
        width: 50%;
        /* border: 3px solid green; */
        padding: 10px;
    }
    td {
        padding: 10px;
    }
    th {
        padding: 10px;
    }
</style>
<body>
    <h1>Data Game</h1>
    <p><a href="{{ route('game.show-store') }}">Tambah Data</a></p>

    <table border="1">
    <tr>
        <th>No</th>
        <th>Tanggal</th>
        <th>Tim 1</th>
        <th>Tim 2</th>
        <th>Aksi</th>
    </tr>
    @php $no=1 @endphp
    @forelse ( $game as $item )
    <tr>
        <td>{{ $no++ }}</td>
        <td>{{ date('d F Y',strtotime($item->mdate) ) }}</td>
        <td>{{ $item->team1 }}</td>
        <td>{{ $item->team2 }}</td>
        <td><a href="{{ url('game/show-update/'.$item->id) }}"><button>Ubah</button></a>
            <button onclick="alertDelete({{ $item->id }});">Hapus</button></td>
    </tr>
    @empty
    <tr>
        <td colspan="4"><center>Data Kosong</center></td>
    </tr>
    @endforelse
    </table>
</body>
<script>
    function alertDelete(id) {
        let text;
        if (confirm("Apakah anda akan menghapus data ini ?") == true) {
            window.location = "{{ url('game/delete') }}/"+id;
        };
    }
</script>
</html>
