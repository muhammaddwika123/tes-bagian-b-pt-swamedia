<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <style>
        .form-login {
            margin: auto;
            width: 50%;
            border: 3px solid green;
            padding: 10px;
        }

        td {
            padding: 5px;
        }

        h1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <div class="form-login">
        <form action="{{ route('game.store') }}" method="POST" >
            {{ csrf_field() }}
        <h1>Tambah Data</h1>
        <center><table>
            <tr>
                <td>Tanggal</td>
                <td><input type="date" id="date" name="date"></td>
            </tr>
            <tr>
                <td>team 1</td>
                <td><input type="text" id="team1" name="team1"></td>
            </tr>
            <tr>
                <td>Tim 2</td>
                <td><input type="text" id="team2" name="team2"></td>
            </tr>
            <tr>
                <td>Stadium</td>
                <td><textarea name="stadium" id="stadium" cols="30" rows="10"></textarea></td>
            </tr>
            <tr>
                <td></td>
                <td><button type="submit">Submit</button> <a href="{{ route('game') }}"><button>Batal</button></a></td>
            </tr>
        </table></center>
    </form>
    </div>

</body>
</html>
