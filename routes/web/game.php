<?php

use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'game',
], function () {
    Route::get('/', [GameController::class, 'index'])->name('game');
    Route::get('/show-store', [GameController::class, 'showCreate'])->name('game.show-store');
    Route::get('/show-update/{id}', [GameController::class, 'showUpdate'])->name('game.show-update');
    Route::post('/create', [GameController::class, 'store'])->name('game.store');
    Route::post('/update', [GameController::class, 'update'])->name('game.update');
    Route::get('/delete/{id}', [GameController::class, 'delete'])->name('game.delete');
});
