<?php

use App\Http\Controllers\API\GameController;
use Illuminate\Support\Facades\Route;

Route::group([
    'prefix' => 'game',
], function () {
    Route::get('/get', [GameController::class, 'get']);
    Route::post('/create', [GameController::class, 'store']);
});
