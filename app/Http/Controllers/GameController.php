<?php

namespace App\Http\Controllers;

use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function index(Request $request) {

        $game = Game::get();

        return view('game.index',compact(
            'game'
        ));
    }

    public function showCreate(Request $request) {

        return view('game.create');
    }

    public function store(Request $request) {

        Game::create([
            'mdate' => $request->date,
            'stadium' => $request->stadium,
            'team1' => $request->team1,
            'team2' => $request->team2
        ]);

        return redirect(route('game'));
    }

    public function showUpdate($id) {

        $data = Game::where('id', $id)->first();

        return view('game.update',compact('data'));
    }

    public function update(Request $request) {

        Game::where('id', $request->id)->update([
            'mdate' => $request->date,
            'stadium' => $request->stadium,
            'team1' => $request->team1,
            'team2' => $request->team2
        ]);

        return redirect(route('game'));
    }

    public function delete($id) {

        Game::where('id',$id)->delete();

        return redirect(route('game'));
    }
}
