<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Models\Game;
use Illuminate\Http\Request;

use App\Http\Resources\GameResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GameController extends Controller
{
    public function get(Request $request) {

        try {
            DB::beginTransaction();

            $limit = 10;
            if (!empty($request->limit)) {
                $limit = $request->limit;
            }

            $game = Game::orderBy('id','DESC');
            if ($request->option == 'select') {
                $game = $game->get();
            } else {
                $game = $game->paginate($limit);
            }

            DB::commit();
            return GameResource::collection($game)->response();
        } catch (\Throwable $th) {
            return response()->json(['message' => $th->getMessage() ],500);
        }

    }

    public function store(Request $request) {

        $rules = [
            'date' =>'required|date_format:Y/m/d',
            'team1' => 'required',
            'team2' =>'required',
            'stadium' =>'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if($validator->fails()){
            return response()->json(['message' => $validator->errors()->first()],500);
        }

        try {
            DB::beginTransaction();

            $data = Game::create([
                'mdate' => $request->date,
                'stadium' => $request->stadium,
                'team1' => $request->team1,
                'team2' => $request->team2,
            ]);

            DB::commit();

            return response()->json(['data' => new GameResource($data) ,'message' => 'Data game berhasil disimpan.'],200);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['message' => $th->getMessage() ],500);
        }
    }
}
